import pandas as pd
import matplotlib.pyplot as plt
import os
import numpy as np

input_Data = '../Results/Data_labels.csv'
input2 = '../Data/part3.csv'
output = '../Results/Merged.csv'
df = pd.read_csv(os.path.abspath(input_Data))
actual = pd.read_csv(os.path.abspath(input2))

# ------------
df_merged = pd.merge(df, actual, how='outer', on='CLIENT_ID')
df_merged.to_csv(os.path.abspath(output), sep=",", index=False)
print(df_merged.head())
df_merged.info()

print('---------------------------')
# Difference
similar = df_merged[df_merged['LABELS'] == df_merged['CLIENT_TYPE']]
difference = df_merged[df_merged['LABELS'] != df_merged['CLIENT_TYPE']]
print(difference)

print(f'There are {similar.shape[0]} correct labels to the actual client')
print(f'There are {difference.shape[0]} labels different from the actual client')

print('------------- Mapping to find client ID -------------------')

# Mapping to find client ID
ID = input('What is client ID?')
client_type = df_merged[df_merged['CLIENT_ID'] == ID]['CLIENT_TYPE']
# else:
#      client_type = 'not appear'
print(f'Customer with ID: {ID} is {client_type}')

print('-----------How many clients in each group----------------')
# How many clients in each group
business = df_merged[df_merged['LABELS'] == 'Business']
healthy = df_merged[df_merged['LABELS'] == 'Healthy']
retirement = df_merged[df_merged['LABELS'] == 'Retirement']
onetime = df_merged[df_merged['LABELS'] == 'Onetime']

print(f'Group Business contains {business.shape[0]} clients, '
      f'Group Healthy contains {healthy.shape[0]} clients, '
      f'Group Retirement contains {retirement.shape[0]} clients, '
      f'Group One time contains {onetime.shape[0]} clients')

print("-------------------The frequency of each group------------------------")
# The frequency of each group
freq = df["LABELS"].value_counts()
percentage = df["LABELS"].value_counts(normalize=True)
d = {"Frequency": freq}
df1 = pd.DataFrame(data=d)
df1['Percentages'] = pd.Series(["{0:.2f}%".format(val * 100) for val in percentage],  index = df1.index)
print(df1)
df1.to_csv('../Results/Customer_freq.csv')

print("---------Each group comes more at which time of the day - lunch/dinner?---------------")

# Each group comes more at which time of the day - lunch/dinner?
df2 = df.groupby(['LABELS', 'TIME']).size().to_frame(
    name='Frequency per group per time').reset_index()  # to make it a dataframe instead of a simple list
df2['%'] = 100 * df2['Frequency per group per time'] / df2.groupby('LABELS')['Frequency per group per time'].transform(
    'sum')
print(df2)
df2.to_csv('../Results/Time_freq.csv')

print("------------------How many times each group consumes each course--------------------")

# Can you determine the likelihood of each group to get a certain course?
# Calculating how many times each group consumes each course
meal = ['FIRST_COURSE', 'SECOND_COURSE', 'THIRD_COURSE']
labels = ['Business', 'Healthy', 'Retirement', 'Onetime']

pct_dict1 = dict()
freq_dict1 = dict()
dtf_pct1 = pd.DataFrame()
dtf_freq1 = pd.DataFrame()

for i in meal:
    for j in labels:
        x = df_merged[df_merged[i] > 0][df_merged['LABELS'] == j].shape[0]
        y = df_merged[df_merged['LABELS'] == j].shape[0]
        #        y = df_merged[df_merged[i] > 0].shape[0]
        pct_dict1[j] = (x / y)
        freq_dict1[j] = (x)
    dtf_pct1 = dtf_pct1.append(pct_dict1, ignore_index=True)
    dtf_freq1 = dtf_freq1.append(freq_dict1, ignore_index=True)
dtf_freq1 = dtf_freq1.set_index(pd.Index(meal))
dtf_pct1 = dtf_pct1.set_index(pd.Index(meal))
print(dtf_freq1)

print('-----------The likelihood for each of these clients to get a certain course----------------')
print(dtf_pct1)

# Can you figure out the probability of each group to get a certain dish?
print('-----------The probability of each group to get a certain dish----------------')

print("-----------The probability of each group to get a certain dish in the first course----")
FirstCourseDishes = df.groupby(['LABELS', '1ST_C_FOOD_NAME']).size().to_frame(
    name='Frequency of a 1C dish per group').reset_index()
FirstCourseDishes['%'] = 100 * FirstCourseDishes['Frequency of a 1C dish per group'] / \
                         FirstCourseDishes.groupby('LABELS')['Frequency of a 1C dish per group'].transform('sum')
print(FirstCourseDishes)
print("-----------The probability of each group to get a certain dish in the second course----")
SecondCourseDishes = df.groupby(['LABELS', '2ND_C_FOOD_NAME']).size().to_frame(
    name='Frequency of a 2C dish per group').reset_index()
SecondCourseDishes['%'] = 100 * SecondCourseDishes['Frequency of a 2C dish per group'] / \
                          SecondCourseDishes.groupby('LABELS')['Frequency of a 2C dish per group'].transform('sum')
print(SecondCourseDishes)
print("-----------The probability of each group to get a certain dish in the third course----")
ThirdCourseDishes = df.groupby(['LABELS', '3RD_C_FOOD_NAME']).size().to_frame(
    name='Frequency of a 3C dish per group').reset_index()
ThirdCourseDishes['%'] = 100 * ThirdCourseDishes['Frequency of a 3C dish per group'] / \
                         ThirdCourseDishes.groupby('LABELS')['Frequency of a 3C dish per group'].transform('sum')
print(ThirdCourseDishes)

# Can you determine the distribution of the costs of the  drinks per course?
print("------------The distribution of the costs of the drinks per course ---------------")
# Plotting the distribution of the costs of drinks in all the courses together
df4 = df[["1ST_C_DRINK", "2ND_C_DRINK", "3RD_C_DRINK"]]
df4.plot.kde(figsize=(10, 8))
plt.show()
# Plotting the distribution of the cost in each course separately
df["1ST_C_DRINK"].plot.kde(title="Distribution of the costs of drinks for the first course", grid=True, figsize=(10, 5),
                           color="tab:blue")
plt.show()
df["2ND_C_DRINK"].plot.kde(title="Distribution of the costs of drinks for the second course", grid=True,
                           figsize=(10, 5), color="tab:orange")
plt.show()
df["3RD_C_DRINK"].plot.kde(title="Distribution the costs of drinks for the third course", grid=True, figsize=(10, 5),
                           color="tab:green")
plt.show()

print('--------------------')
