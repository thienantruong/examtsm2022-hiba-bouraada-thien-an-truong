import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import os
from sklearn.cluster import KMeans

input1 = '../Results/Data.csv'
df = pd.read_csv(os.path.abspath(input1))
output = '../Results/Data_labels.csv'

np.random.seed(5)

x = df[["FIRST_COURSE", "SECOND_COURSE", "THIRD_COURSE"]].values

kmeans = KMeans(n_clusters=4, init='k-means++', max_iter=300, n_init=10, random_state=0)
y_kmeans = kmeans.fit_predict(x)

plt.scatter(x[y_kmeans == 0, 0], x[y_kmeans == 0, 1], c='purple', label='Cluster 1')
plt.scatter(x[y_kmeans == 1, 0], x[y_kmeans == 1, 1], c='orange', label='Cluster 2')
plt.scatter(x[y_kmeans == 2, 0], x[y_kmeans == 2, 1], c='green', label='Cluster 3')
plt.scatter(x[y_kmeans == 3, 0], x[y_kmeans == 3, 1], c='blue', label='Cluster 4')

# Plotting the centroids of the clusters
plt.scatter(kmeans.cluster_centers_[:, 0], kmeans.cluster_centers_[:, 1], c='red', label='Centroids')

plt.legend()

# 3d scatter plot using matplotlib

fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111, projection='3d')
ax.scatter(x[y_kmeans == 0, 0], x[y_kmeans == 0, 1], x[y_kmeans == 0, 2], c='purple', label='Cluster 1')
ax.scatter(x[y_kmeans == 1, 0], x[y_kmeans == 1, 1], x[y_kmeans == 1, 2], c='orange', label='Cluster 2')
ax.scatter(x[y_kmeans == 2, 0], x[y_kmeans == 2, 1], x[y_kmeans == 2, 2], c='green', label='Cluster 3')
ax.scatter(x[y_kmeans == 3, 0], x[y_kmeans == 3, 1], x[y_kmeans == 3, 2], c='blue', label='Cluster 4')
# Plotting the centroids of the clusters
ax.scatter(kmeans.cluster_centers_[:, 0], kmeans.cluster_centers_[:, 1], s=50, c='red', label='Centroids')
ax.set(xlabel="FIRST COURSE", ylabel="SECOND COURSE", zlabel="THIRD COURSE")
plt.show()

LABELS = []
for group in y_kmeans:
    if group == 0:
        LABELS.append("Business")
    elif group == 1:
        LABELS.append("Healthy")
    elif group == 2:
        LABELS.append("Retirement")
    else:
        LABELS.append("Onetime")

df['CLUSTER_GROUP'] = y_kmeans
df['LABELS'] = LABELS

print(df.head(20))
df.to_csv(os.path.abspath(output), sep=",", index=False)