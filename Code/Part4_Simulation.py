import os
import numpy as np
import pandas as pd
import random
customer_freq = '../Results/Customer_freq.csv'
time_freq = '../Results/Time_freq.csv'
df_customer_freq = pd.read_csv(os.path.abspath(customer_freq))
df_time_freq = pd.read_csv(os.path.abspath(time_freq))

print(df_time_freq)

labels = {'Business': [], 'Healthy':[], 'Retirement':[], 'Onetime':[]}

return_prob = {'Business':0.5, 'Healthy': 0.7, 'Retirement': 0.9, 'Onetime': 0}
customer_prob = (pd.to_numeric(df_customer_freq.iloc[:,2])/100)

customer_type = []
n = 0

for i in range(5*365):
    while n < 20:
        customer_type = np.random.choice(('Business', 'Healthy', 'Retirement', 'Onetime'), p = customer_prob)

        if not labels[customer_type]:
            labels[customer_type].append(f'ID{np.random.randint(10**6)}')
            k = np.random.random()
        if k < return_prob[customer_type]:
            _id = np.random.choice(labels[customer_type])
        else:  # new_customer
            _id = (f'ID{np.random.randint(10**6)}')
            labels[customer_type].append(_id)
        if labels[customer_type] == 'Business':
            time = np.random.choice(('LUNCH', 'DINNER'), p=((df_time_freq.loc[0,'%'])/100,(df_time_freq.loc[1,'%'])/100))
            labels[customer_type].append(time)
        else labels[customer_type] == 'Healthy':
        n = n+1

print(labels)









